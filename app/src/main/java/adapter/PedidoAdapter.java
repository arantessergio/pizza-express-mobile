package adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.sergio.pizzaexpress.R;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import model.ESituacaoPedido;
import model.Item;
import model.Pedido;

/**
 * Created by Sergio on 13/08/2015.
 */
public class PedidoAdapter extends BaseAdapter {

    private Context context;
    private List<Pedido> pedidos;

    public PedidoAdapter(Context context, List<Pedido> pedidos) {
        this.context = context;
        this.pedidos = pedidos;
    }

    @Override
    public int getCount() {
        return pedidos.size();
    }

    @Override
    public Object getItem(int position) {
        return pedidos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return pedidos.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rootView = convertView.inflate(context, R.layout.adapter_pedidos, null);

        TextView lblAtendente = (TextView) rootView.findViewById(R.id.lblAtendentePedidoAdapter);
        TextView lblDataRealizacao = (TextView) rootView.findViewById(R.id.lblDataRealizacaoPedidoAdapter);
        TextView lblEntregador = (TextView) rootView.findViewById(R.id.lblEntregadorPedidoAdapter);
        TextView lblFormaPagamento = (TextView) rootView.findViewById(R.id.lblFormaPagamentoPedidoAdapter);
        TextView lblHoraEntrega = (TextView) rootView.findViewById(R.id.lblHoraEntregaPedidoAdapter);
        TextView lblNumeroPedido = (TextView) rootView.findViewById(R.id.lblNumeroPedidoAdapter);
        TextView lblSituacao = (TextView) rootView.findViewById(R.id.lblSituacaoPedidoAdapter);
        TextView lblTotal = (TextView) rootView.findViewById(R.id.lblTotalPedidoAdapter);
        TextView btnVerItens = (TextView) rootView.findViewById(R.id.btnVerItensPedidoAdapter);

        Pedido pedido = pedidos.get(position);
        String nomeAtendente = pedido.getAtendente().getNome();
        Date dataRealizacao = pedido.getData();
        String nomeEntregador = pedido.getMotoboy().getNome();
        String formaPagamento = pedido.getFormaPagamento().getDescricao();
        Date horaEntrega = pedido.getHoraEntrega();
        String numero = pedido.getNumero();
        ESituacaoPedido situacao = pedido.getSituacao();
        BigDecimal total = pedido.getTotal();
        final List<Item> itens = pedido.getItens();

        lblAtendente.setText(nomeAtendente);
        lblDataRealizacao.setText(dataRealizacao.toString());
        lblEntregador.setText(nomeEntregador);
        lblFormaPagamento.setText(formaPagamento);
        lblHoraEntrega.setText(horaEntrega.toString());
        lblNumeroPedido.setText(numero);
        lblSituacao.setText(situacao.toString());
        lblTotal.setText(total.toString());

        if(position % 2 == 0) {
            rootView.setBackgroundColor(Color.LTGRAY);
        }

        btnVerItens.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogItens(itens);
            }
        });

        return rootView;
    }

    private void showDialogItens(List<Item> itens) {
        //TODO abrir um dialog com os itens do pedido
        Toast.makeText(context, "Itens: hue hue hue hue", Toast.LENGTH_SHORT).show();
    }
}
