package adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sergio.pizzaexpress.R;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import model.Item;
import model.Sabor;

/**
 * Created by Sergio on 14/08/2015.
 */
public class PedidoItensAdapter extends BaseAdapter {

    private Context context;
    private List<Item> itens;

    public PedidoItensAdapter(Context context, List<Item> itens) {
        this.context = context;
        this.itens = itens;
    }

    @Override
    public int getCount() {
        return itens.size();
    }

    @Override
    public Object getItem(int position) {
        return itens.get(position);
    }

    @Override
    public long getItemId(int position) {
        return itens.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rootView = convertView.inflate(context, R.layout.adapter_item_pedido, null);

        TextView lblPrecoUnitario = (TextView) rootView.findViewById(R.id.lblPrecoUnitarioItemAdapter);
        TextView lblProduto = (TextView)rootView.findViewById(R.id.lblProdutoItemAdapter);
        TextView lblQuantidade = (TextView)rootView.findViewById(R.id.lblQuantidadeItemAdapter);
        TextView lblTotal = (TextView)rootView.findViewById(R.id.lblTotalItemAdapter);
        TextView lblSabores = (TextView)rootView.findViewById(R.id.lblSaboresItemAdapter);

        Item item = itens.get(position);
        String nomeProduto = item.getProduto().getNome();
        BigDecimal precoUnitario = item.getProduto().getPrecoVenda();
        Long quantidade = item.getQuantidade();
        BigDecimal total = item.getTotal();
        List<Sabor> sabores = item.getSabores();
        StringBuilder builder = new StringBuilder();
        for (Iterator<Sabor> iterator = sabores.iterator(); iterator.hasNext(); ) {
            Sabor sabor = iterator.next();
            builder.append(sabor.getDescricao());
            if(iterator.hasNext()) {
                builder.append(" - ");
            }
        }

        lblPrecoUnitario.setText(precoUnitario.toString());
        lblProduto.setText(nomeProduto);
        lblQuantidade.setText(quantidade.toString());
        lblTotal.setText(total.toString());
        lblSabores.setText(builder.toString());

        return rootView;
    }
}
