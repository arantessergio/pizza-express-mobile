package adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sergio.pizzaexpress.R;

import java.math.BigDecimal;
import java.util.List;

import model.Item;
import model.Sabor;

/**
 * Created by Sergio on 14/08/2015.
 */
public class CarrinhoAdapter extends BaseAdapter {

    private Context context;
    private List<Item> items;

    public CarrinhoAdapter(Context context, List<Item> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return items.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rootView = convertView.inflate(context, R.layout.adapter_carrinho, null);

        TextView lblNome = (TextView)rootView.findViewById(R.id.lblNomeProdutoAdapterCarrinho);
        TextView lblQuantidade = (TextView)rootView.findViewById(R.id.lblQuantidadeCarrinhoAdapter);
        TextView lblPrecoUnit = (TextView)rootView.findViewById(R.id.lblPrecoUnitAdapterCarrinho);
        TextView lblTotal = (TextView)rootView.findViewById(R.id.lblTotalProdutoAdapterCarrinho);
        View btnVerSabores = rootView.findViewById(R.id.btnSaboresCarrinhoAdapter);

        Item item = items.get(position);

        String nome = item.getProduto().getNome();
        Long quantidade = item.getQuantidade();
        BigDecimal total = item.getTotal();
        BigDecimal precoUnit = item.getProduto().getPrecoVenda();
        List<Sabor> sabores = item.getSabores();

        lblNome.setText(nome);
        lblQuantidade.setText(String.valueOf(quantidade));
        lblPrecoUnit.setText(precoUnit.toString());
        lblTotal.setText(item.getTotal().toString());

        btnVerSabores.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abrirDialogSabores();
            }
        });

        return rootView;
    }

    private void abrirDialogSabores() {

    }
}
