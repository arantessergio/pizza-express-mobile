package adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sergio.pizzaexpress.R;

import java.util.List;

import model.novo.Sabor;

/**
 * Created by Sergio on 12/08/2015.
 */
public class SaborAdapter extends BaseAdapter {

    private Context context;
    private List<Sabor> sabores;

    public SaborAdapter(Context context, List<Sabor> sabores) {
        this.context = context;
        this.sabores = sabores;
    }

    @Override
    public int getCount() {
        return sabores.size();
    }

    @Override
    public Object getItem(int position) {
        return sabores.get(position);
    }

    @Override
    public long getItemId(int position) {
        return sabores.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rootView = convertView.inflate(context, R.layout.adapter_sabores, null);

        Sabor sabor = sabores.get(position);
        String descricao = sabor.getDescricao();
        String descricaoCompleta = sabor.getDescricaoCompleta();

        TextView txtDesc = (TextView) rootView.findViewById(R.id.txtSaborAdapter);

        txtDesc.setText(descricao != null ? descricao : "");

        TextView txtDescricaoCompleta = (TextView) rootView.findViewById(R.id.txtSituacaoSaborAdapter);

        txtDescricaoCompleta.setText(descricaoCompleta != null ? descricaoCompleta : "");

        return rootView;
    }
}
