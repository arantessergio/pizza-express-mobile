package util;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;

import model.novo.Configuracao;
import model.novo.ETamanhoPizza;

/**
 * Created by sergio.junior on 11/11/2015.
 */
public class ConfiguracaoUtil extends AsyncTask<Void, Void, BigDecimal> {

    private Context context;
    private ETamanhoPizza tamanhoPizza;
    private ProgressDialog dialog;
    private BigDecimal valor;

    public ConfiguracaoUtil(Context context, ETamanhoPizza tamanhoPizza) {
        this.context = context;
        this.tamanhoPizza = tamanhoPizza;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = new ProgressDialog(context);
        dialog.setMessage("Carregando configurações de preços");
        dialog.show();
    }

    @Override
    protected BigDecimal doInBackground(Void... voids) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
        Configuracao result = restTemplate.getForObject("http://pizzaexpress-ajr.rhcloud.com/api/configuracao", Configuracao.class);
        switch (tamanhoPizza) {
            case PEQUENA:
                return result.getValorPequena();
            case MEDIA:
                return result.getValorMedia();
            case GRANDE:
                return result.getValorGrande();
            case GIGANTE:
                return result.getValorGigante();
            default:
                return BigDecimal.ZERO;
        }
    }

    @Override
    protected void onPostExecute(BigDecimal bigDecimal) {
        valor = bigDecimal;
        dialog.dismiss();
    }

    public BigDecimal getValor() {
        return valor;
    }
}
