package util;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import cos.ItemCO;
import cos.PedidoCO;
import model.Bebida;
import model.Item;
import model.Pedido;
import model.Sabor;

/**
 * Created by Sergio on 03/09/2015.
 */
public class FechaPedidoUtil {

    public static void fecharPedido(Pedido pedido, Context context, String url){
        //monta o JSON
        int idFormaPagamento = pedido.getFormaPagamento() != null ? pedido.getFormaPagamento().getId().intValue() : 0;
        int idEntregador = pedido.getMotoboy() != null ? pedido.getMotoboy().getId().intValue() : 0;
        double totalPedido = pedido.getTotal() != null ? pedido.getTotal().doubleValue() : BigDecimal.ZERO.doubleValue();
        int idAtendente = pedido.getAtendente() != null ? pedido.getAtendente().getId().intValue() : 0;
        String dataPedidoString = pedido.getData() != null ? pedido.getData().toString() : "";
        String horaRealizacaoString = pedido.getHoraRealizacao() != null ? pedido.getHoraRealizacao().toString() : "";
        String horaSaidaString = pedido.getHoraSaida() != null ? pedido.getHoraSaida().toString() : "";
        String numeroPedido = pedido.getNumero() != null ? pedido.getNumero() : "";
        String situacaoPedido = pedido.getSituacao() != null ? pedido.getSituacao().toString() : "";
        int idEstabelecimento = pedido.getEstabelecimento() != null ? pedido.getEstabelecimento().getId().intValue() : 0;

        ItemCO[] itensJSON = new ItemCO[]{};
        for (int i = 0; i < pedido.getItens().size(); i++) {
            Item item = pedido.getItens().get(i);
            Sabor sabor01 = item.getSabores().get(0);
            Sabor sabor02 = item.getSabores().get(1);
            Sabor sabor03 = item.getSabores().get(2);
            Sabor sabor04 = item.getSabores().get(3);

            int quantidadeBebida = item.getQuantidadeBebida() != null ? item.getQuantidadeBebida().intValue() : 0;
            int quantidadePizza = item.getQuantidade() != null ? item.getQuantidade().intValue() : 0;

            int idBebida = item.getBebida() != null ? item.getBebida().getId().intValue() : 0;

            int idSabor1 = sabor01 != null ? sabor01.getId().intValue() : 0;
            int idSabor2 = sabor02 != null ? sabor02.getId().intValue() : 0;
            int idSabor3 = sabor03 != null ? sabor03.getId().intValue() : 0;
            int idSabor4 = sabor04 != null ? sabor04.getId().intValue() : 0;

            double totalItem = item.getTotal() != null ? item.getTotal().doubleValue() : 0;

            int tamanhoPizza = item.getTamanhoPizza() != null ? item.getTamanhoPizza().ordinal() : 0;

            ItemCO itemCO = new ItemCO(tamanhoPizza, idSabor1, idSabor2, idSabor3, idSabor4, idBebida, quantidadePizza, quantidadeBebida, totalPedido);

            itensJSON[i] = itemCO;
        }

        PedidoCO pedidoCO = new PedidoCO(0, dataPedidoString, horaSaidaString, horaRealizacaoString, horaSaidaString, numeroPedido, situacaoPedido,
                String.valueOf(totalPedido), idAtendente, idEstabelecimento, idEntregador, idFormaPagamento, itensJSON);

        Gson gson = new Gson();

        String json = gson.toJson(pedidoCO);

        url += "?json=";
        url += json;



        //TODO fazer a chamada da URL e passar esse JSOn




    }


    private class EnviarPedidoServer extends AsyncTask<Void, Void, PedidoCO> {

        private String url;
        private Spinner spinner;
        private ProgressDialog dialog;
        private Context context;


        public EnviarPedidoServer(Context context, String url, Spinner spinner) {
            this.context = context;
            this.context = context;
            this.url = url;
            this.spinner = spinner;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(context);
            dialog.setMessage("Carregando...");
            dialog.show();
        }

        @Override
        protected PedidoCO doInBackground(Void... params) {
            return getBebidasFromServer();
        }

        @Override
        protected void onPostExecute(PedidoCO pedidoCO) {
            super.onPostExecute(pedidoCO);
            int layout = android.R.layout.simple_list_item_1;
            /*ArrayAdapter<Bebida> adapter = new ArrayAdapter<Bebida>(context, layout, bebidas);
            spinner.setAdapter(adapter);*/
            dialog.dismiss();
        }

        private PedidoCO getBebidasFromServer() {

            final PedidoCO[] pedidoCO = new PedidoCO[1];

            JsonArrayRequest req = new JsonArrayRequest(url,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            Log.d("Response JSON: ", response.toString());
                            try {
                                //String jsonResponse = "";
                                for (int i = 0; i < response.length(); i++) {

                                    JSONObject objectJson = null;
                                    try {
                                        objectJson = (JSONObject) response.get(i);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    int idPedido = objectJson.getInt("idPedido");
                                    String data = objectJson.getString("data");
                                    String horaEntrega = objectJson.getString("horaEntrega");
                                    String horaRealizacao = objectJson.getString("horaRealizacao");
                                    String horaSaira = objectJson.getString("horaSaira");
                                    String numero = objectJson.getString("numero");
                                    String situacao = objectJson.getString("situacao");
                                    String total = objectJson.getString("total");
                                    int idAtendente = objectJson.getInt("idAtendente");
                                    int idEstabelecimento = objectJson.getInt("idEstabelecimento");
                                    int idEntregador = objectJson.getInt("idEntregador");
                                    int idFormaPagamento = objectJson.getInt("idFormaPagamento");
                                    JSONArray itemCOs = objectJson.getJSONArray("itemCOs");

                                    ItemCO[] itemCOs1 = new ItemCO[itemCOs.length()];

                                    for (int j = 0; j < itemCOs.length() ; j++) {
                                        JSONObject o = (JSONObject) itemCOs.get(i);

                                        int tamanhoPizza = o.getInt("tamanhoPizza");
                                        int idSabor01 = o.getInt("idSabor01");
                                        int idSabor02 = o.getInt("idSabor02");
                                        int idSabor03 = o.getInt("idSabor03");
                                        int idSabor04 = o.getInt("idSabor04");
                                        int idBebida = o.getInt("idBebida");
                                        int quantidade = o.getInt("quantidade");
                                        int quantidadeBebida = o.getInt("quantidadeBebida");
                                        double totalItem = o.getDouble("total");

                                        ItemCO itemCO = new ItemCO(tamanhoPizza, idSabor01, idSabor02, idSabor03,
                                                idSabor04, idBebida, quantidade, quantidadeBebida, totalItem);
                                        itemCOs1[i] = itemCO;
                                    }

                                    pedidoCO[0] = new PedidoCO(idPedido, data, horaEntrega, horaRealizacao, horaSaira, numero,
                                            situacao, total, idAtendente, idEstabelecimento, idEntregador, idFormaPagamento, itemCOs1);

                                }
                                /*return pedidoCO;*/
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(context,
                                        "Error: " + e.getMessage(),
                                        Toast.LENGTH_LONG).show();
                                try {
                                    throw e;
                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                }
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d("Error parse json", "Error: " + error.getMessage());
                    Toast.makeText(context,
                            "Nenhum dado recebido!", Toast.LENGTH_SHORT).show();
                    error.printStackTrace();
                }
            });

            AppController.getInstance().addToRequestQueue(req);

            return pedidoCO[0];
        }
    }

}

