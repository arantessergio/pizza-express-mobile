package util;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import adapter.SaborAdapter;
import model.novo.Sabor;

public class LoadSabores extends AsyncTask<Void, Void, List<Sabor>> {

    private Context context;
    private String url;
    private ListView listView;
    private ProgressDialog dialog;

    public LoadSabores(Context context, String url, ListView listView) {
        this.context = context;
        this.url = url;
        this.listView = listView;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = new ProgressDialog(context);
        dialog.setMessage("Carregando...");
        dialog.show();
    }

    @Override
    protected List<Sabor> doInBackground(Void... params) {
        return getSaboresFromServer();
    }

    @Override
    protected void onPostExecute(List<Sabor> sabores) {
        super.onPostExecute(sabores);
        SaborAdapter adapter = new SaborAdapter(context, sabores);
        listView.setAdapter(adapter);
        dialog.dismiss();
    }

    private List<Sabor> getSaboresFromServer() {
        final List<Sabor> sabores = new ArrayList<>();
        JsonArrayRequest req = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d("Response JSON: ", response.toString());
                        try {
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject objectJson = null;
                                try {
                                    objectJson = (JSONObject) response.get(i);
                                } catch (JSONException e) {e.printStackTrace();}
                                long id = objectJson.getLong("id");
                                String descricao = objectJson.getString("descricao");
                                Sabor sabor = new Sabor();
                                sabor.setId(id);
                                sabor.setDescricao(descricao);
                                sabores.add(sabor);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(context, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error parse json", "Error: " + error.getMessage());
                Toast.makeText(context, "Nenhum dado recebido!", Toast.LENGTH_SHORT).show();
                error.printStackTrace();
            }
        });
        AppController.getInstance().addToRequestQueue(req);
        return sabores;
    }
}