package model;

import java.io.Serializable;

public class Endereco implements Serializable {

	private static final long serialVersionUID = 9098025200482524601L;
	private Long id;
	private String rua;
	private Long numero;
	private Bairro bairro;
	private Cidade cidade;
	private Estado estado;
	private Localizacao localizacao;

	private Boolean semNumero;

	public Endereco() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRua() {
		return rua;
	}

	public void setRua(String rua) {
		this.rua = rua;
	}

	public Long getNumero() {
		return numero;
	}

	public void setNumero(Long numero) {
		this.numero = numero;
	}

	public Bairro getBairro() {
		return bairro;
	}

	public void setBairro(Bairro bairro) {
		this.bairro = bairro;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public Localizacao getLocalizacao() {
		return localizacao;
	}

	public void setLocalizacao(Localizacao localizacao) {
		this.localizacao = localizacao;
	}

	public Boolean getSemNumero() {
		return semNumero;
	}

	public void setSemNumero(Boolean semNumero) {
		this.semNumero = semNumero;
	}

}
