package model;

/**
 * Created by Sergio on 30/08/2015.
 */
public class Bebida {

    private Long id;
    private String descricao;
    private Double valor;

    public Bebida() {
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Bebida{" +
                "descricao='" + descricao + '\'' +
                ", valor=" + valor +
                '}';
    }
}
