package model.novo;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by sergio.junior on 11/11/2015.
 */
public class Bebida implements Serializable {

    private Long id;
    private String descricao;
    private BigDecimal valorCusto;
    private BigDecimal valorVenda;

    public Bebida() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public BigDecimal getValorCusto() {
        return valorCusto;
    }

    public void setValorCusto(BigDecimal valorCusto) {
        this.valorCusto = valorCusto;
    }

    public BigDecimal getValorVenda() {
        return valorVenda;
    }

    public void setValorVenda(BigDecimal valorVenda) {
        this.valorVenda = valorVenda;
    }
}
