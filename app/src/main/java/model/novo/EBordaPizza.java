package model.novo;

/**
 * Created by sergio.junior on 11/11/2015.
 */
public enum EBordaPizza {

    SEM_BORDA_RECHEADA {
        @Override
        public String toString() {
            return "Sem borda";
        }
    },
    CATUPIRY {
        @Override
        public String toString() {
            return "Catupiry";
        }
    },
    CHEDDAR {
        @Override
        public String toString() {
            return "Cheddar";
        }
    },
    BORDA_DOCE {
        @Override
        public String toString() {
            return "Borda doce";
        }
    }

}
