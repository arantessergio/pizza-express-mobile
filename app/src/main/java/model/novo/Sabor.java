package model.novo;

import java.io.Serializable;

/**
 * Created by sergio.junior on 11/11/2015.
 */
public class Sabor implements Serializable {

    private Long id;
    private String descricao;
    private Boolean situacao;
    private String descricaoCompleta;

    public Sabor() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Boolean getSituacao() {
        return situacao;
    }

    public void setSituacao(Boolean situacao) {
        this.situacao = situacao;
    }

    public String getDescricaoCompleta() {
        return descricaoCompleta;
    }

    public void setDescricaoCompleta(String descricaoCompleta) {
        this.descricaoCompleta = descricaoCompleta;
    }

    @Override
    public String toString() {
        return descricao + "\n" + descricaoCompleta;
    }
}
