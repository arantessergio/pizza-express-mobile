package model.novo;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by sergio.junior on 11/11/2015.
 */
public class Configuracao implements Serializable {

    private Long id;
    private BigDecimal valorFrete;
    private BigDecimal valorPequena;
    private BigDecimal valorMedia;
    private BigDecimal valorGrande;
    private BigDecimal valorGigante;
    private BigDecimal valorGiganteBorda;

    public Configuracao() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getValorFrete() {
        return valorFrete;
    }

    public void setValorFrete(BigDecimal valorFrete) {
        this.valorFrete = valorFrete;
    }

    public BigDecimal getValorPequena() {
        return valorPequena;
    }

    public void setValorPequena(BigDecimal valorPequena) {
        this.valorPequena = valorPequena;
    }

    public BigDecimal getValorMedia() {
        return valorMedia;
    }

    public void setValorMedia(BigDecimal valorMedia) {
        this.valorMedia = valorMedia;
    }

    public BigDecimal getValorGrande() {
        return valorGrande;
    }

    public void setValorGrande(BigDecimal valorGrande) {
        this.valorGrande = valorGrande;
    }

    public BigDecimal getValorGigante() {
        return valorGigante;
    }

    public void setValorGigante(BigDecimal valorGigante) {
        this.valorGigante = valorGigante;
    }

    public BigDecimal getValorGiganteBorda() {
        return valorGiganteBorda;
    }

    public void setValorGiganteBorda(BigDecimal valorGiganteBorda) {
        this.valorGiganteBorda = valorGiganteBorda;
    }
}
