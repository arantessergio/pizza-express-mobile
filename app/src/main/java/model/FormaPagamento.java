package model;

import java.io.Serializable;
import java.util.List;

public class FormaPagamento implements Serializable {

    private static final long serialVersionUID = -6730283310282785856L;
    private Long id;
    private String descricao;
    private List<Pedido> pedidos;

    public FormaPagamento() {
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getDescricao() {
	return descricao;
    }

    public void setDescricao(String descricao) {
	this.descricao = descricao;
    }

    public List<Pedido> getPedidos() {
	return pedidos;
    }

    public void setPedidos(List<Pedido> pedidos) {
	this.pedidos = pedidos;
    }

}
