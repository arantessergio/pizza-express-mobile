package model;

import java.io.Serializable;

public class Sabor implements Serializable {

	private static final long serialVersionUID = -2944199585396562220L;
	private Long id;
	private String descricao;
	private Boolean situacao;
	private Item item;

	public Sabor() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public Boolean getSituacao() {
		return situacao;
	}

	public void setSituacao(Boolean situacao) {
		this.situacao = situacao;
	}

	@Override
	public String toString() {
		return "Sabor{" +
				"descricao='" + descricao + '\'' +
				", situacao=" + situacao +
				'}';
	}
}
