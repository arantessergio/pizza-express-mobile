package model;

import java.io.Serializable;

public class Localizacao implements Serializable {

	private static final long serialVersionUID = 7298429337091270144L;
	private Long id;
	private Double latitude;
	private Double longitude;
	private Endereco endereco;
	private Entregador motoboy;

	public Localizacao() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public Entregador getMotoboy() {
		return motoboy;
	}

	public void setMotoboy(Entregador motoboy) {
		this.motoboy = motoboy;
	}

}
