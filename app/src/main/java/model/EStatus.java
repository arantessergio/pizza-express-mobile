package model;

public enum EStatus {

	ATIVO {
		@Override
		public String toString() {
			return "Ativo";
		}
	},
	INATIVO {
		@Override
		public String toString() {
			return "Inativo";
		}
	};

}
