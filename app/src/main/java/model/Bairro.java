package model;

import java.io.Serializable;
import java.util.List;

public class Bairro implements Serializable {

	private static final long serialVersionUID = 3545439858127923759L;
	private Long id;
	private String nome;

	private Cidade cidade;

	private List<Endereco> enderecos;

	public Bairro() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public List<Endereco> getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(List<Endereco> enderecos) {
		this.enderecos = enderecos;
	}

}
