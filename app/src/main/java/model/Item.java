package model;

import com.orm.SugarRecord;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class Item extends SugarRecord<Item> implements Serializable {

	private static final long serialVersionUID = 9079311555810475984L;
	private Long id;
	private Pedido pedido;
	private Produto produto;
	private Bebida bebida;
	private Long quantidadeBebida;
	private Long quantidade;
	private BigDecimal total;
	private List<Sabor> sabores;
	private ETamanhoPizza tamanhoPizza;
	private Long[] saboresArray;
	private Long bebidaId;

	public Item() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public Long getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Long quantidade) {
		this.quantidade = quantidade;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public List<Sabor> getSabores() {
		return sabores;
	}

	public void setSabores(List<Sabor> sabores) {
		this.sabores = sabores;
	}

	public ETamanhoPizza getTamanhoPizza() {
		return tamanhoPizza;
	}

	public void setTamanhoPizza(ETamanhoPizza tamanhoPizza) {
		this.tamanhoPizza = tamanhoPizza;
	}

	public Bebida getBebida() {
		return bebida;
	}

	public Long getQuantidadeBebida() {
		return quantidadeBebida;
	}

	public void setQuantidadeBebida(Long quantidadeBebida) {
		this.quantidadeBebida = quantidadeBebida;
	}

	public Long[] getSaboresArray() {
		return saboresArray;
	}

	public void setSaboresArray(Long[] saboresArray) {
		this.saboresArray = saboresArray;
	}

	public Long getBebidaId() {
		return bebidaId;
	}

	public void setBebidaId(Long bebidaId) {
		this.bebidaId = bebidaId;
	}

	public void setBebida(Bebida bebida) {
		this.bebida = bebida;
	}
}
