package model;

import java.io.Serializable;

public class Telefone implements Serializable {

	private static final long serialVersionUID = -8111946266949136318L;
	private Long id;
	private ETipoTelefone tipo;
	private Long ddd;
	private Long numero;
	private Estabelecimento estabelecimento;
	private Cliente cliente;
	private Entregador motoboy;
	private Atendente atendente;

	public Telefone() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ETipoTelefone getTipo() {
		return tipo;
	}

	public void setTipo(ETipoTelefone tipo) {
		this.tipo = tipo;
	}

	public Long getDdd() {
		return ddd;
	}

	public void setDdd(Long ddd) {
		this.ddd = ddd;
	}

	public Long getNumero() {
		return numero;
	}

	public void setNumero(Long numero) {
		this.numero = numero;
	}

	public Estabelecimento getEstabelecimento() {
		return estabelecimento;
	}

	public void setEstabelecimento(Estabelecimento estabelecimento) {
		this.estabelecimento = estabelecimento;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Entregador getMotoboy() {
		return motoboy;
	}

	public void setMotoboy(Entregador motoboy) {
		this.motoboy = motoboy;
	}

	public Atendente getAtendente() {
		return atendente;
	}

	public void setAtendente(Atendente atendente) {
		this.atendente = atendente;
	}

}
