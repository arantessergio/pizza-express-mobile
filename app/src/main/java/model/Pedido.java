package model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class Pedido implements Serializable {

	private static final long serialVersionUID = 8604504216267166586L;
	private Long id;
	private String numero;
	private Cliente cliente;
	private Entregador motoboy;
	private Atendente atendente;
	private List<Item> itens;
	private Estabelecimento estabelecimento;
	private FormaPagamento formaPagamento;
	private Date data;
	private Date horaEntrega;
	private Date horaRealizacao;
	private Date horaSaida;
	private BigDecimal total;
	private ESituacaoPedido situacao;

	public Pedido() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Entregador getMotoboy() {
		return motoboy;
	}

	public void setMotoboy(Entregador motoboy) {
		this.motoboy = motoboy;
	}

	public Atendente getAtendente() {
		return atendente;
	}

	public void setAtendente(Atendente atendente) {
		this.atendente = atendente;
	}

	public ESituacaoPedido getSituacao() {
		return situacao;
	}

	public void setSituacao(ESituacaoPedido situacao) {
		this.situacao = situacao;
	}

	public List<Item> getItens() {
		return itens;
	}

	public void setItens(List<Item> itens) {
		this.itens = itens;
	}

	public Estabelecimento getEstabelecimento() {
		return estabelecimento;
	}

	public void setEstabelecimento(Estabelecimento estabelecimento) {
		this.estabelecimento = estabelecimento;
	}

	public FormaPagamento getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(FormaPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Date getHoraEntrega() {
		return horaEntrega;
	}

	public void setHoraEntrega(Date horaEntrega) {
		this.horaEntrega = horaEntrega;
	}

	public Date getHoraRealizacao() {
		return horaRealizacao;
	}

	public void setHoraRealizacao(Date horaRealizacao) {
		this.horaRealizacao = horaRealizacao;
	}

	public Date getHoraSaida() {
		return horaSaida;
	}

	public void setHoraSaida(Date horaSaida) {
		this.horaSaida = horaSaida;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

}
