package model;

import java.io.Serializable;
import java.util.List;

public class Estado implements Serializable {

	private static final long serialVersionUID = 3336349470717277825L;
	private Long id;
	private String nome;
	private EUf uf;
	private List<Cidade> cidades;
	private List<Endereco> enderecos;

	public Estado() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public EUf getUf() {
		return uf;
	}

	public void setUf(EUf uf) {
		this.uf = uf;
	}

	public List<Cidade> getCidades() {
		return cidades;
	}

	public void setCidades(List<Cidade> cidades) {
		this.cidades = cidades;
	}

	public List<Endereco> getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(List<Endereco> enderecos) {
		this.enderecos = enderecos;
	}

}
