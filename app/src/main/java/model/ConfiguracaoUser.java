package model;

import com.orm.SugarRecord;

public class ConfiguracaoUser extends SugarRecord<ConfiguracaoUser>{

    private Long id;
    private String rua;
    private String bairro;
    private Long numero;
    private String telefone;

    public ConfiguracaoUser() {
    }

    public ConfiguracaoUser(String rua, String bairro, Long numero, String telefone) {
        this.rua = rua;
        this.bairro = bairro;
        this.numero = numero;
        this.telefone = telefone;
    }

    public Long getId() {
        return id;
    }

    public String getRua() {
        return rua;
    }

    public String getBairro() {
        return bairro;
    }

    public Long getNumero() {
        return numero;
    }

    public String getTelefone() {
        return telefone;
    }
}
