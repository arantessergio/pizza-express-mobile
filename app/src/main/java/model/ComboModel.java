package model;

public class ComboModel {

	private Long id;
	private String descricao;

	public ComboModel(Long id, String descricao) {
		super();
		this.id = id;
		this.descricao = descricao;
	}

	public Long getId() {
		return id;
	}

	public String getDescricao() {
		return descricao;
	}

}
