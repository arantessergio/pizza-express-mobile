package exception;

/**
 * Created by Sergio on 29/08/2015.
 */
public class CamposInvalidosException extends RuntimeException {

    public CamposInvalidosException(String message) {
        super(message);
    }
}
