package dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import model.ETamanhoPizza;
import model.Item;

/**
 * Created by Sergio on 31/08/2015.
 */
public class CarrinhoDao extends SQLiteOpenHelper {

    private static final String DATABASE = "PIZZA_EXPRESS";
    private static final int VERSION = 2;

    public CarrinhoDao(Context context) {
        super(context, DATABASE, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        StringBuilder builder = new StringBuilder();
        builder.append("CREATE TABLE carrinho")
                .append("(")
                .append("_id INTEGER PRIMARY KEY AUTOINCREMENT, ")
                .append("tamanhopizza INTEGER, ")
                .append("quantidadepizza INTEGER, ")
                .append("idsabor01 INTEGER, ")
                .append("idsabor02 INTEGER, ")
                .append("idsabor03 INTEGER, ")
                .append("idsabor04 INTEGER, ")
                .append("idbebida INTEGER, ")
                .append("quantidadebebida INTEGER")
                .append(")");

        db.execSQL(builder.toString());

    }

    public void salvar(Item item){

        ContentValues values = new ContentValues();
        values.put("tamanhopizza", item.getTamanhoPizza().ordinal());
        values.put("quantidadepizza", item.getQuantidade());
        values.put("idsabor01", item.getSaboresArray()[0]);
        values.put("idsabor02", item.getSaboresArray()[1]);
        values.put("idsabor03", item.getSaboresArray()[2]);
        values.put("idsabor04", item.getSaboresArray()[3]);
        values.put("idbebida", item.getBebida().getId());
        values.put("quantidadebebida", item.getQuantidadeBebida());

        getWritableDatabase().insert("carrinho", null, values);

    }

    public List<Item> listar() {
        List<Item> items = new ArrayList<Item>();

        String[] colunas = {"_id", "tamanhopizza", "quantidadepizza", "idsabor01",
        "idsabor02", "idsabor03", "idsabor04", "idbebida", "quantidadebebida"};

        Cursor cursor = getWritableDatabase().query("carrinho", colunas, null, null, null, null, null);

        while (cursor.moveToNext()) {
            Item item = new Item();
            item.setId(cursor.getLong(0));
            int i = cursor.getInt(1);
            item.setTamanhoPizza(i == 0 ? ETamanhoPizza.PEQUENA : i == 1 ? ETamanhoPizza.MEDIA
            : i == 2 ? ETamanhoPizza.GRANDE : ETamanhoPizza.GIGANTE);
            item.setQuantidade(cursor.getLong(2));
            item.setSaboresArray(new Long[]{cursor.getLong(3), cursor.getLong(4), cursor.getLong(5), cursor.getLong(6)});
            item.setBebidaId(cursor.getLong(7));
            item.setQuantidadeBebida(cursor.getLong(8));
            items.add(item);
        }

        return items;
    }

    public void limparCarrinho(){
        String sql = "DELETE FROM carrinho";
        getWritableDatabase().execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS carrinho");
        this.onCreate(db);
    }
}
