package activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.sergio.pizzaexpress.R;

import java.util.List;

import exception.CamposInvalidosException;
import model.ConfiguracaoUser;

/**
 * Created by Sergio on 30/07/2015.
 */
public class FragmentSettings extends Fragment {

    private EditText txtBairro;
    private EditText txtRua;
    private EditText txtNumero;
    private EditText txtTelefone;
    private Button btnSalvar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_settings, container, false);

        txtBairro = (EditText) rootView.findViewById(R.id.txtBairro);
        txtRua = (EditText) rootView.findViewById(R.id.txtRua);
        txtNumero = (EditText) rootView.findViewById(R.id.txtNumero);
        txtTelefone = (EditText) rootView.findViewById(R.id.txtTelefone);

        btnSalvar = (Button) rootView.findViewById(R.id.btnSalvarConfig);

        carregaDadosIniciais();

        configureActions();

        return rootView;
    }

    private void configureActions() {
        btnSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    validaCampos();
                    //validaCamposNovo(new EditText[]{txtNumero, txtRua, txtBairro, txtTelefone});
                    ConfiguracaoUser config = new ConfiguracaoUser(txtRua.getText().toString(),
                            txtBairro.getText().toString(),
                            Long.valueOf(txtNumero.getText().toString()),
                            txtTelefone.getText().toString());
                    new SalvarConfiguracaoTask(getActivity(), config).execute();
                } catch (CamposInvalidosException e) {
                    if (e instanceof CamposInvalidosException) {
                        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(),
                                "Erro desconhecido, favor entrar em contato com nosso suporte!", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });
    }

    private void carregaDadosIniciais() {
        List<ConfiguracaoUser> list = ConfiguracaoUser.listAll(ConfiguracaoUser.class);
        if(list.size() > 0) {
            ConfiguracaoUser config = list.get(0);
            if(config != null) {
                String bairro = config.getBairro();
                Long numero = config.getNumero();
                String rua = config.getRua();
                String telefone = config.getTelefone();

                this.txtRua.setText(rua != null ? rua : "");
                this.txtBairro.setText(bairro != null ? bairro : "");
                this.txtTelefone.setText(telefone != null ? telefone : "");
                this.txtNumero.setText(String.valueOf(numero));
            }
        }
    }

    private class SalvarConfiguracaoTask extends AsyncTask<Void, Void, Void> {

        private Context context;
        private ConfiguracaoUser config;
        private ProgressDialog dialog;

        public SalvarConfiguracaoTask(Context context, ConfiguracaoUser config) {
            this.context = context;
            this.config = config;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(context);
            dialog.setMessage("Aguarde...");
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            if(ConfiguracaoUser.listAll(ConfiguracaoUser.class).size() > 0) {
                ConfiguracaoUser.deleteAll(ConfiguracaoUser.class);
            }
            config.save();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dialog.dismiss();
            limparCampos();
            Toast.makeText(context, "Dados atualizados com sucesso!", Toast.LENGTH_SHORT).show();
        }
    }

    private void validaCamposNovo(EditText[] editTexts) {
        EditText[] camposComErro = new EditText[editTexts.length];
        for (int i = 0; i < editTexts.length; i++) {
            EditText editText = editTexts[i];
            if(editText.getText().toString().equals("") || editText.getText().toString() == null) {
                camposComErro[i] = editText;
            }
        }

        for (int i = 0; i < camposComErro.length; i++){
            EditText editText = camposComErro[i];
            editText.setBackgroundColor(Color.rgb(255, 99, 71));
        }

        if(camposComErro.length > 0) {
            Toast.makeText(getActivity(), "Existem campos não preenchidos, verifique!", Toast.LENGTH_SHORT).show();
        }

    }

    private void validaCampos() {
        if(txtBairro.getText().toString() != null || txtRua.getText().toString() != null
                || txtNumero.getText().toString() != null || txtTelefone.getText().toString() != null) {
            if(txtRua.getText().toString().equals("") || txtBairro.getText().toString().equals("")
                    || txtTelefone.getText().toString().equals("") || txtNumero.getText().toString().equals("")) {
                throw new CamposInvalidosException("Existem campos não preenchidos, verifique!");
            }
        }
    }

    private void limparCampos() {

        this.txtBairro.setText("");
        this.txtNumero.setText("");
        this.txtRua.setText("");
        this.txtTelefone.setText("");

    }

}
