package activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.sergio.pizzaexpress.R;

import util.LoadSabores;



public class SaboresFragment extends Fragment {

    private ListView listagem;

    public SaboresFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sabores, container, false);
        listagem = (ListView) rootView.findViewById(R.id.lstSabores);
        String url = "http://192.168.103.111:8080/pizzaria/api/sabores";
        new LoadSabores(getActivity(), url, listagem).execute();

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}