package activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.sergio.pizzaexpress.R;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import adapter.SaborAdapter;
import dao.CarrinhoDao;
import model.novo.ETamanhoPizza;
import model.novo.EBordaPizza;
import model.novo.Pizza;
import model.novo.Sabor;
import util.ConfiguracaoUtil;

/**
 * Created by Sergio on 31/07/2015.
 */
public class ItensFragment extends Fragment {

    private Button btnConfirmar;
    private Spinner spnSabor01;
    private Spinner spnSabor02;
    private Spinner spnSabor03;
    private Spinner spnSabor04;

    private Spinner spnTamanho;
    private EditText txtQuantidade;

    private EditText txtObservacao;

    private CarrinhoDao dao;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_itens, container, false);

        spnTamanho = (Spinner) rootView.findViewById(R.id.spnTamanhoPizza);
        txtQuantidade = (EditText) rootView.findViewById(R.id.txtQuantidade);

        //spinners sabores
        spnSabor01 = (Spinner) rootView.findViewById(R.id.spnSabor01);
        spnSabor02 = (Spinner) rootView.findViewById(R.id.spnSabor02);
        spnSabor03 = (Spinner) rootView.findViewById(R.id.spnSabor03);
        spnSabor04 = (Spinner) rootView.findViewById(R.id.spnSabor04);

        txtObservacao = (EditText) rootView.findViewById(R.id.txtObservacaoItem);

        btnConfirmar = (Button) rootView.findViewById(R.id.btnConfirmarItemCarrinho);

        final Spinner spnBorda = (Spinner) rootView.findViewById(R.id.spnBorda);

        dao = new CarrinhoDao(getActivity());

        int layout = android.R.layout.simple_list_item_1;
        ArrayAdapter<ETamanhoPizza> adapter = new ArrayAdapter<ETamanhoPizza>(getActivity(), layout, ETamanhoPizza.values());
        spnTamanho.setAdapter(adapter);

        EBordaPizza[] values = EBordaPizza.values();
        ArrayAdapter<EBordaPizza> adapterBorda = new ArrayAdapter<EBordaPizza>(getActivity(), layout, values);
        spnBorda.setAdapter(adapterBorda);

        carregarSpinnerSabores();


        final TextView lblSabor02 = (TextView) rootView.findViewById(R.id.lblSabor02);
        final TextView lblSabor03 = (TextView) rootView.findViewById(R.id.lblSabor03);
        final TextView lblSabor04 = (TextView) rootView.findViewById(R.id.lblSabor04);

        spnTamanho.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ETamanhoPizza tamanho = (ETamanhoPizza) spnTamanho.getSelectedItem();
                switch (tamanho) {
                    case PEQUENA:
                        //Toast.makeText(getActivity(), "Você tem direito a 1(UM) sabor!", Toast.LENGTH_LONG).show();
                        disableEnableSpinners(false, View.GONE, new TextView[]{lblSabor02, lblSabor03, lblSabor04}, new Spinner[]{spnSabor02, spnSabor03, spnSabor04});
                        break;
                    case MEDIA:
                        //Toast.makeText(getActivity(), "Você tem direito a 2(DOIS) sabores!", Toast.LENGTH_LONG).show();
                        disableEnableSpinners(false, View.GONE, new TextView[]{lblSabor03, lblSabor04}, new Spinner[]{spnSabor03, spnSabor04});
                        disableEnableSpinners(true, View.VISIBLE, new TextView[]{lblSabor02}, spnSabor02);
                        break;
                    case GRANDE:
                        //Toast.makeText(getActivity(), "Você tem direito a 3(TRÊS) sabores!", Toast.LENGTH_LONG).show();
                        disableEnableSpinners(false, View.GONE, new TextView[]{lblSabor04}, spnSabor04);
                        disableEnableSpinners(true, View.VISIBLE, new TextView[]{lblSabor02, lblSabor03}, spnSabor02, spnSabor03);
                        break;
                    case GIGANTE:
                        //Toast.makeText(getActivity(), "Você tem direito a 4(QUATRO) sabores!", Toast.LENGTH_LONG).show();
                        disableEnableSpinners(true, View.VISIBLE, new TextView[]{lblSabor02, lblSabor03, lblSabor04}, new Spinner[]{spnSabor02, spnSabor03, spnSabor04});
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnConfirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Sabor sabor01 = (Sabor) spnSabor01.getSelectedItem();
                Sabor sabor02 = (Sabor) spnSabor02.getSelectedItem();
                Sabor sabor03 = (Sabor) spnSabor03.getSelectedItem();
                Sabor sabor04 = (Sabor) spnSabor04.getSelectedItem();
                String textoQuantidade = txtQuantidade.getText().toString();
                ETamanhoPizza tamanho = (ETamanhoPizza) spnTamanho.getSelectedItem();
                EBordaPizza borda = (EBordaPizza) spnBorda.getSelectedItem();
                String observacao = txtObservacao.getText().toString();

                ArrayList<Sabor> sabores = new ArrayList<Sabor>();

                switch (tamanho) {
                    case PEQUENA:
                        sabores.add(sabor01);
                        break;
                    case MEDIA:
                        sabores.add(sabor01);
                        if (sabor01.getId().compareTo(sabor02.getId()) != 0) {
                            sabores.add(sabor02);
                        }
                        break;
                    case GRANDE:
                        sabores.add(sabor01);
                        if (sabor01.getId().compareTo(sabor02.getId()) != 0) {
                            sabores.add(sabor02);
                            if (sabor01.getId().compareTo(sabor03.getId()) != 0) {
                                if(sabor02.getId().compareTo(sabor03.getId()) != 0) {
                                    sabores.add(sabor03);
                                }
                            }
                        }
                        break;
                    case GIGANTE:
                        sabores.add(sabor01);
                        if (sabor01.getId().compareTo(sabor02.getId()) != 0) {
                            sabores.add(sabor02);
                            if (sabor01.getId().compareTo(sabor03.getId()) != 0) {
                                if(sabor02.getId().compareTo(sabor03.getId()) != 0) {
                                    sabores.add(sabor03);
                                    if(sabor01.getId().compareTo(sabor04.getId()) != 0){
                                        if(sabor02.getId().compareTo(sabor04.getId()) != 0){
                                            if(sabor03.getId().compareTo(sabor04.getId()) != 0){
                                                sabores.add(sabor04);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        break;
                }

                Pizza pizza = new Pizza();
                pizza.setBorda(borda);
                pizza.setObservacao(observacao);
                pizza.setQuantidade(Long.valueOf(textoQuantidade));
                pizza.setSabores(sabores);
                pizza.setTamanhoPizza(tamanho);
                ConfiguracaoUtil util = new ConfiguracaoUtil(getActivity(), tamanho);
                util.execute();
                BigDecimal total = util.getValor();
                pizza.setTotal(total);


            }
        });

        return rootView;
    }

    private void limparCampos() {

        this.txtQuantidade.setText("");

    }

    private void disableEnableSpinners(boolean enabled, int
            visibility, TextView[] textViews, Spinner... spinners) {
        for (Spinner s : spinners) {
            s.setEnabled(enabled);
            s.setVisibility(visibility);
        }
        for (TextView t : textViews) {
            t.setVisibility(visibility);
        }
    }

    private void carregarSpinnerSabores() {
        Spinner[] spinners = new Spinner[4];
        spinners[0] = spnSabor01;
        spinners[1] = spnSabor02;
        spinners[2] = spnSabor03;
        spinners[3] = spnSabor04;
        new TarefaCarregarSabores(getActivity(), "http://pizzaexpress-ajr.rhcloud.com/api/sabores", spinners).execute();
    }

    private class TarefaCarregarSabores extends AsyncTask<Void, Void, Sabor[]> {

        private Context context;
        private String url;
        private Spinner[] spinner;
        private ProgressDialog dialog;

        public TarefaCarregarSabores(Context context, String url, Spinner[] spinner) {
            this.context = context;
            this.url = url;
            this.spinner = spinner;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(context);
            dialog.setMessage("Carregando sabores...");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected Sabor[] doInBackground(Void... voids) {

            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<Sabor[]> responseEntity = restTemplate.getForEntity(url, Sabor[].class);
            Sabor[] sabores = (Sabor[]) responseEntity.getBody();

            return sabores;
        }

        @Override
        protected void onPostExecute(Sabor[] sabors) {
            ArrayList<Sabor> sabores = new ArrayList<Sabor>();
            for (Sabor s : sabors) {
                sabores.add(s);
            }
            SaborAdapter saborAdapter = new SaborAdapter(context, sabores);
            for (Spinner s : spinner) {
                s.setAdapter(saborAdapter);
            }
            dialog.dismiss();
        }
    }

}