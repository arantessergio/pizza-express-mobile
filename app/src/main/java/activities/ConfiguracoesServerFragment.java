package activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.sergio.pizzaexpress.R;

/**
 * Created by Sergio on 03/08/2015.
 */
public class ConfiguracoesServerFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.config_server_frame, container, false);

        final EditText txtIp = (EditText) rootView.findViewById(R.id.txtIp);
        final EditText txtPorta = (EditText) rootView.findViewById(R.id.txtPorta);
        final Button btnSalvar = (Button) rootView.findViewById(R.id.btnSalvar);

        btnSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    //ConfigConnections config = new ConfigConnections();
                    //config.setIp(txtIp.getText().toString());
                    //config.setPorta(txtPorta.getText().toString());
                    //if(ConfigConnections.listAll(ConfigConnections.class).size() > 0) {
                    //   ConfigConnections.deleteAll(ConfigConnections.class);
                    //}
                    //Thread.sleep(10000);
                    //config.save();
                    Toast.makeText(getActivity(), "Configurações aplicadas com sucesso!", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "ERRO: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });


        return rootView;
    }
}
