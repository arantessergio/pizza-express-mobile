package activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.sergio.pizzaexpress.R;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import adapter.CarrinhoAdapter;
import model.Item;
import model.Produto;
import model.Sabor;


public class MeuCarrinhoFragment extends Fragment {

    public MeuCarrinhoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_meu_carrinho, container, false);

        Produto produto = new Produto();
        produto.setNome("Produtinho maroto");
        produto.setPrecoVenda(BigDecimal.ONE);

        Sabor sabor = new Sabor();
        sabor.setDescricao("Calabreza");

        List<Sabor> sabores = new ArrayList<>();
        sabores.add(sabor);
        Item item = new Item();

        item.setTotal(BigDecimal.TEN);
        item.setSabores(sabores);
        item.setQuantidade(BigDecimal.ONE.longValue());
        item.setProduto(produto);

        List<Item> items = new ArrayList<>();
        items.add(item);


        ListView viewById = (ListView) rootView.findViewById(R.id.lstItems);
        //int layout = android.R.layout.simple_list_item_1;
        //ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), layout, items);
        CarrinhoAdapter adapter = new CarrinhoAdapter(getActivity(), items);
        viewById.setAdapter(adapter);
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}