package activities;

import android.annotation.TargetApi;
import android.graphics.Outline;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.widget.TextView;

import com.sergio.pizzaexpress.R;

public class HomeFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        //fazer chegar o nome do usuario logado
        TextView txtUsuarioLogado = (TextView) rootView.findViewById(R.id.lblUsuarioLogado);

        String logado = getActivity().getIntent().getStringExtra("USUARIO_LOGADO");

        txtUsuarioLogado.setText(logado != null ? logado : "");

        View carrinho = rootView.findViewById(R.id.add_button);
        carrinho.setOutlineProvider(new ViewOutlineProvider() {
            @Override
            public void getOutline(View view, Outline outline) {
                int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.diameter);
                outline.setOval(0,0,dimensionPixelSize, dimensionPixelSize);
            }
        });
        carrinho.setClipToOutline(true);

        carrinho.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abrirTelaItens();
            }
        });

        return rootView;
    }

    private void abrirTelaItens() {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.container, new ItensFragment());
        transaction.commit();
    }

}
