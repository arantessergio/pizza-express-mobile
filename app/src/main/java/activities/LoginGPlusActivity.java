package activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.sergio.pizzaexpress.R;

/**
 * Created by Sergio on 31/07/2015.
 */
public class LoginGPlusActivity extends Activity implements
        View.OnClickListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener{

    private static final int RC_SIGN_IN = 0;

    private static final String TAG = "LoginGPlusActivity";

    private static final int PROFILE_PIC_SIZE = 400;

    private GoogleApiClient googleApiClient;

    private ConnectionResult mConnectionResult;

    private boolean mIntentProgress;

    private boolean mSignInClicked;

    private SignInButton btnSignIn;
    private String nomeUsuario;


    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        setContentView(R.layout.login_gplus_frame);
        btnSignIn = (SignInButton) findViewById(R.id.btn_sign_in);

        btnSignIn.setOnClickListener(this);
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API)
                .addScope(Plus.SCOPE_PLUS_LOGIN).build();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == RC_SIGN_IN) {
            if(resultCode != RESULT_OK) {
                mSignInClicked = false;
            }
            mIntentProgress = false;
            if(!googleApiClient.isConnecting()){
                googleApiClient.connect();
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API)
                .addScope(Plus.SCOPE_PLUS_LOGIN).build();

        if(googleApiClient != null) {
            googleApiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(googleApiClient.isConnected()) {
            googleApiClient.disconnect();
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        new TarefaLogin(this).execute();
    }


    private void conectado() {
        mSignInClicked = false;
        nomeUsuario = Plus.PeopleApi.getCurrentPerson(googleApiClient).getDisplayName();
        Toast.makeText(this, "Bem vindo " + (nomeUsuario != null ? nomeUsuario : ""), Toast.LENGTH_SHORT).show();
        showProfileInformation();
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("USUARIO_LOGADO", nomeUsuario);
        startActivity(intent);
        this.finish();
    }

    private void updateUI(boolean signedIn) {
        if(signedIn) {
            btnSignIn.setVisibility(View.GONE);
        } else {
            btnSignIn.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        googleApiClient.connect();
        updateUI(false);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

        if(!connectionResult.hasResolution()) {
            GooglePlayServicesUtil.getErrorDialog(connectionResult.getErrorCode(), this, 0).show();
            return;
        }

        if(!mIntentProgress) {
            mConnectionResult = connectionResult;
            resolverErrosDeLogin();
        }

    }

    private void resolverErrosDeLogin() {
        if(mConnectionResult.hasResolution()) {
            try {
                mIntentProgress = true;
                mConnectionResult.startResolutionForResult(this, RC_SIGN_IN);
            } catch (IntentSender.SendIntentException e) {
                //e.printStackTrace();
                mIntentProgress = false;
                googleApiClient.connect();
            }
        }
    }

    @Override
    public void onClick(View v) {

        if(v.getId() == R.id.btn_sign_in) {
            loginComGooglePlus();
        }

    }

    private void loginComGooglePlus() {
        if(!googleApiClient.isConnecting()) {
            mSignInClicked = true;
            resolverErrosDeLogin();
        }
    }

    private void logout() {
        //montar a estrutura de logout
        if (googleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(googleApiClient);
            googleApiClient.disconnect();
            googleApiClient.connect();
            updateUI(false);
        }

    }

    private void showProfileInformation () {
        if(Plus.PeopleApi.getCurrentPerson(googleApiClient) != null) {
            Person usuarioLogado = Plus.PeopleApi.getCurrentPerson(googleApiClient);
            nomeUsuario = usuarioLogado.getDisplayName();
            //Toast.makeText(this, "Usuario logado: " + usuarioLogado.getDisplayName() + ", Email: " + usuarioLogado.getUrl(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Não foi possível fazer a conexão" , Toast.LENGTH_LONG).show();
        }
    }

    private class TarefaLogin extends AsyncTask<Void, Void, Void> {

        private Context context;
        private ProgressDialog progressDialog;

        public TarefaLogin(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Efetuando login, Aguarde :)");
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            conectado();
        }
    }

}
