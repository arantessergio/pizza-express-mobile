package activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.sergio.pizzaexpress.R;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import adapter.PedidoAdapter;
import model.Atendente;
import model.ESituacaoPedido;
import model.Entregador;
import model.FormaPagamento;
import model.Item;
import model.Pedido;
import model.Produto;
import model.Sabor;


public class MeusPedidosFragment extends Fragment {

    public MeusPedidosFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_meus_pedidos, container, false);
        ListView listagemPedidos = (ListView) rootView.findViewById(R.id.lstPedidos);
        List<Pedido> lista = new ArrayList<Pedido>();
        Atendente atendente = new Atendente();
        atendente.setNome("Atendente maroto");
        Entregador entregador = new Entregador();
        entregador.setNome("Entregador manolo");
        FormaPagamento formaPagamento = new FormaPagamento();
        formaPagamento.setDescricao("A vista");

        Produto p = new Produto();
        p.setDescricao("Produto maroto");
        p.setPrecoVenda(BigDecimal.valueOf(20));

        List<Item> itens = new ArrayList<Item>();

        for (int i = 0; i < 5; i++) {
            Item item = new Item();
            item.setProduto(p);
            item.setQuantidade(Long.valueOf(2));
            item.setSabores(new ArrayList<Sabor>());
            item.setTotal(BigDecimal.valueOf(20));
        }

        for (int i = 0; i < 140; i++) {
            //lista.add("Pedido de numero " + i);
            Pedido pedido = new Pedido();
            pedido.setAtendente(atendente);
            pedido.setMotoboy(entregador);
            pedido.setNumero("12345" + i);
            pedido.setData(new Date());
            pedido.setHoraEntrega(new Date());
            pedido.setFormaPagamento(formaPagamento);
            pedido.setItens(itens);
            lista.add(pedido);
            pedido.setSituacao(ESituacaoPedido.ENTREGUE);
            pedido.setTotal(BigDecimal.TEN);
        }



        //int layout = android.R.layout.simple_list_item_1;
        //ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), layout, lista);
        //new PedidoAdapter(pedi)
        listagemPedidos.setDividerHeight(4);
        PedidoAdapter adapter = new PedidoAdapter(getActivity(), lista);
        listagemPedidos.setAdapter(adapter);
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}