package activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.sergio.pizzaexpress.R;

import java.util.ArrayList;
import java.util.List;

import model.Bebida;
import model.Item;

/**
 * Created by Sergio on 29/08/2015.
 */
public class BebidaFragment extends Fragment {


    private Spinner spnBebida;
    private EditText txtQuantidade;
    private Button btnAdicionar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.bebida_fragment, container);

        spnBebida = (Spinner) rootView.findViewById(R.id.spnBebida);
        txtQuantidade = (EditText) rootView.findViewById(R.id.txtQuantidade);
        btnAdicionar = (Button) rootView.findViewById(R.id.btnAdicionarBebida);

        carregaBebidas();

        configureActions();

        return rootView;
    }

    private void carregaBebidas() {
        int layout = android.R.layout.simple_list_item_1;
        List<Bebida> bebidas = new ArrayList<Bebida>();
        for(int i = 0; i < 10; i++) {
            Bebida bebida = new Bebida();
            bebida.setDescricao("Bebida " + i);
            bebida.setValor(Double.valueOf(i));
            bebidas.add(bebida);
        }
        //String[] bebidas = {"COCA-COLA", "KUAT", "FANTA", "SUCO_LARANJA"};
        ArrayAdapter<Bebida> adapter = new ArrayAdapter<Bebida>(getActivity(), layout, bebidas);
        this.spnBebida.setAdapter(adapter);
    }

    private void configureActions() {
        btnAdicionar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Item item = new Item();
                Bebida bebida = (Bebida) spnBebida.getSelectedItem();
                item.setBebida(bebida);
                item.setQuantidade(Long.valueOf(txtQuantidade.getText().toString()));
            }
        });
    }
}
