package cos;

import java.io.Serializable;

public class PedidoCO implements Serializable{

	private int id;
	private String data;
	private String horaEntrega;
	private String horaRealizacao;
	private String horaSaida;
	private String numero;
	private String situacao;
	private String total;
	private int idAtendente;
	private int idEstabelecimento;
	private int idEntregador;
	private int idFormaPagamento;
	private ItemCO[] itemCOs;

	public PedidoCO(int id, String data, String horaEntrega, String horaRealizacao, String horaSaida,
					String numero, String situacao, String total,
					int idAtendente, int idEstabelecimento, int idEntregador,
					int idFormaPagamento, ItemCO[] itemCOs) {
		this.id = id;
		this.data = data;
		this.horaEntrega = horaEntrega;
		this.horaRealizacao = horaRealizacao;
		this.horaSaida = horaSaida;
		this.numero = numero;
		this.situacao = situacao;
		this.total = total;
		this.idAtendente = idAtendente;
		this.idEstabelecimento = idEstabelecimento;
		this.idEntregador = idEntregador;
		this.idFormaPagamento = idFormaPagamento;
		this.itemCOs = itemCOs;
	}

	public PedidoCO() {
	}

	public int getId() {
		return id;
	}

	public String getData() {
		return data;
	}

	public String getHoraEntrega() {
		return horaEntrega;
	}

	public String getHoraRealizacao() {
		return horaRealizacao;
	}

	public String getHoraSaida() {
		return horaSaida;
	}

	public String getNumero() {
		return numero;
	}

	public String getSituacao() {
		return situacao;
	}

	public String getTotal() {
		return total;
	}

	public int getIdAtendente() {
		return idAtendente;
	}

	public int getIdEstabelecimento() {
		return idEstabelecimento;
	}

	public int getIdEntregador() {
		return idEntregador;
	}

	public int getIdFormaPagamento() {
		return idFormaPagamento;
	}

	public ItemCO[] getItemCOs() {
		return itemCOs;
	}
}
