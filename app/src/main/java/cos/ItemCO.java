package cos;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Sergio on 02/09/2015.
 */
public class ItemCO implements Serializable{

    private int tamanhoPizza;
    private int idSabor01;
    private int idSabor02;
    private int idSabor03;
    private int idSabor04;
    private int idBebida;
    private int quantidade;
    private int quantidadeBebida;
    private double total;

    public ItemCO(int tamanhoPizza, int idSabor01, int idSabor02,
                  int idSabor03, int idSabor04, int idBebida, int quantidade,
                  int quantidadeBebida, double total) {
        this.tamanhoPizza = tamanhoPizza;
        this.idSabor01 = idSabor01;
        this.idSabor02 = idSabor02;
        this.idSabor03 = idSabor03;
        this.idSabor04 = idSabor04;
        this.idBebida = idBebida;
        this.quantidade = quantidade;
        this.quantidadeBebida = quantidadeBebida;
        this.total = total;
    }

    public int getTamanhoPizza() {
        return tamanhoPizza;
    }

    public int getIdSabor01() {
        return idSabor01;
    }

    public int getIdSabor02() {
        return idSabor02;
    }

    public int getIdSabor03() {
        return idSabor03;
    }

    public int getIdSabor04() {
        return idSabor04;
    }

    public int getIdBebida() {
        return idBebida;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public int getQuantidadeBebida() {
        return quantidadeBebida;
    }

    public double getTotal() {
        return total;
    }
}
